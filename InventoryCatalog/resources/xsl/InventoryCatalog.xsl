<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="4.0" indent="yes" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"/>
<xsl:template match="//dataroot" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8"/>
<title>Consumable Inventory List</title>
<link rel="stylesheet" type="text/css" href="js/jquery.dataTables.min.css"></link>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type= "text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/subdata.js"></script>
<style type="text/css" class="init"> 
		td.details-control {
		background: url('images/details_open.png') no-repeat center center;
		cursor: pointer;
		}
		tr.shown td.details-control {
		background: url('images/details_close.png') no-repeat center center;
		}
</style>
</head>
<body link="#OcOOOO" vlink="#050000">
	<h2>Lab Operations Squadron - Consumables Inventory</h2>
	<h3>Last Updated: <xsl:value-of select="@generated"/></h3>
	<table border="l" bgcolor="#ffffff" cellspacing="O" cellpadding="O" id="CTRL1" class="display" style="width:100%;">
<thead>
<!--HEADER-->
</thead>
	<tbody id="CTRL1">
		<xsl:for-each select="Inventoryitem">
		<!--Sentinel-->
		</xsl:for-each>
	</tbody>
<tfoot>
<!--FOOTER -->
</tfoot>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>