function format(d) {
// 'd' is the original data object for the row
return '<table cellpadding="S" cellspacing= "O" border="O" style= "padding-left :50px;">'+
'<tr>'+
'<td>NOTE:</td>'+
'<td>'+d[12]+'</td>'+
'</tr>'+
'<tr>'+
'<td>Min Threshold :</td>'+
'<td>'+d[13]+'</td>'+
'</tr>'+
'<tr>'+
'<td>ODR QTY:</td>'+
'<td>'+d[23]+ '</td>'+
'</tr>'+
'<tr>'+
'<td>FY18 BUY:</td>'+
'<td>'+d[24]+'</td>'+
'</tr>'+ 
'</table>';
}

	$(document).ready(function() {
	var table = $('#CTRL1').DataTable({
	// "ajax ": "../ajax/data/objects.txt ",
	
	"columnDefs": [
		{
			"targets": [ 12 ],
			"visible": false
		},
		{
			"targets": [ 13 ],
			"visible": false
		},
		{
			"targets": [ 23 ],
			"visible": false
		},
		{
			"targets": [ 24 ],
			"visible": false
		}
	],
	"order": [[4, 'asc']]
	});
	// Add event listener for opening and closing details
	$('#CTRL1 tbody').on('click', 'td.details-control', function() {
	var tr = $(this).closest('tr');
	var row= table.row(tr);

	if(row.child.isShown()) {
		// This row is already open - close it 
		row.child.hide();
		tr.rernoveClass('shown');
	}
	else {
		// Open this row
		var rdata = row.data();
		row.child(forrnat(rdata)).show();
		tr.addClass('shown');
	}
});
});