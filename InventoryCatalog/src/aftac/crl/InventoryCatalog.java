package aftac.crl;

import java.io.*;
import java.util.*;
import javax.swing.*;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.InvalidXPathException;
import org.dom4j.Node;
import org.dom4j.Visitor;
import org.dom4j.XPath;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class InventoryCatalog {

    public static final FileNameExtensionFilter FILTER_EXCEL;
    public static final FileNameExtensionFilter FILTER_XML;
    public static final FileNameExtensionFilter FILTER_HTML;

    public static final int COUNT = 30;

    static {
        FILTER_EXCEL = new FileNameExtensionFilter("MS Excel Files", "xlsx", "xls", "xlsm");
        FILTER_XML = new FileNameExtensionFilter("XML File", "xml");
        FILTER_HTML = new FileNameExtensionFilter("Web page", "html", "htm", "xhtml");
    }

    public static void main(String[] args) {
        // User chooses .xlsx file
        // Read inventory items row & cell of spreadsheet
        // Write out xml file
        InventoryCatalog ic = new InventoryCatalog();
        File input = ic.getFileName("Select Input File", FILTER_EXCEL);

        if (input != null) {
            List<InventoryItem> list = ic.readItems(input);
            File output = ic.getFileName("Select Output File", FILTER_XML);
            if (output != null) {
                ic.writeItemsXML((Iterator<Row>) list, output);
            }
        }
    }

    public File getFileName(String inTitle, FileNameExtensionFilter infilter) {
        return this.getFileName(inTitle, new JButton(), infilter);
    }

    public File getFileName(String inTitle, JButton inbutton, FileNameExtensionFilter inFilter) {
        File retval = null;

        JFileChooser chooseFile = new JFileChooser();
        chooseFile.setDialogTitle(inTitle);
        chooseFile.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooseFile.addChoosableFileFilter(inFilter);

        if (chooseFile.showOpenDialog(inbutton) == JFileChooser.APPROVE_OPTION) {
            File rFile = chooseFile.getSelectedFile();
            System.out.println("Selected file: " + rFile.getAbsolutePath());
            retval = rFile;
        } else {
            System.out.println("No file selected");
        }
        return retval;
    }

    public List<InventoryItem> readItems(File sourceFile) {
        List<InventoryItem> retval = new ArrayList<>();
        try {
            if (sourceFile.canRead()) {
                OPCPackage pkg = OPCPackage.open(sourceFile);
                XSSFWorkbook workbook = new XSSFWorkbook(pkg);
                XSSFSheet sheet = workbook.getSheetAt(0);
                XSSFRow row;
                XSSFRow rowData;
                XSSFCell cell;
                Map<Integer, ColumnInfo> ciMap = new HashMap<>();
                int i = 0;
                int fR = sheet.getFirstRowNum();

                while (i <= sheet.getLastRowNum()) {
                    rowData = sheet.getRow(i);
                    switch (i) {
                        case 0:
                            ciMap = this.getColumnInfoMap(rowData);
                            break;
                        default:
                            InventoryItem item = new InventoryItem(rowData, ciMap);
                            retval.add(item);
                            break;
                    }
                    i++;
                }
            } else {
                System.out.println("File unreadable..");
            }
        } catch (IOException | InvalidFormatException e) {
            System.out.println(e);
        }
        return retval;
    }

    public Iterator<Row> getRowIterator(File inputFile) {
        Iterator<Row> retval = null;

        if (inputFile.canRead()) {
            try {
                OPCPackage pkg = OPCPackage.open(inputFile);
                XSSFWorkbook workbook = new XSSFWorkbook(pkg);
                XSSFSheet sheet = workbook.getSheetAt(0);
                retval = sheet.rowIterator();
            } catch (IOException | InvalidFormatException ex) {
                Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retval;
    }

    public boolean writeItemsXML(Iterator<Row> rowIterator, File inOutput) {
        boolean retval = false;
        try {
            Row tempRow = rowIterator.next();
            Map<Integer, ColumnInfo> ciMap = getColumnInfoMap(tempRow);
            InventoryItem tempItem = new InventoryItem(ciMap);
            Date date = new Date();
            File xslOut = getTargetXSL(inOutput);
            writeItemsXSL(inOutput, ciMap);
            Document document = DocumentHelper.createDocument();
            localProcessingInstruction pi = new localProcessingInstruction();
            pi.setTarget("xml-stylesheet");
            pi.setValue("type", "text/xsl");
            pi.setValue("href", "xsl/" + xslOut.getName());
            document.add(pi);
            pi.setDocument(document);
            Element root = document.addElement("dataroot")
                    .addAttribute("xm1ns:od", "urn:schemas-microsoft-com:officedata")
                    .addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                    .addAttribute("xsi:noNamespaceSchemaLocation", "TL%20ALL.xsd")
                    .addAttribute("generated", date.toString());

            // 1.  Get header/column info.
            // 2.  Get the first row from the iterator.
            // 3. Construct column info map.
            // 4. For each row populate s	inventoery item object.
            // 5. For each row write the xml object/element 
            
            while (rowIterator.hasNext()) {
                tempRow = rowIterator.next();
                if (tempItem.setValues(tempRow)) {
                    tempItem.writeXML(root, ciMap);
                }
            }
            OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter writer = new XMLWriter(new FileOutputStream(inOutput), format);
            writer.write(document);
            retval = true;
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        return retval;
    }

    public void writeXSL(PrintWriter pw, Map<Integer, ColumnInfo> inMap) {
        pw.println("\t\t<tr>");
        pw.println("\t\t\t\t<td align=\"center\" class=\"details-control\"></td>");
        for (ColumnInfo i : inMap.values()) {
            pw.printf("\t\t\t\t<td align=\"center\"><xsl:value-of "
                    + "select=\"%s\"/><ltd>\n", i.elementName);
        }
        pw.println("\t\t</tr>");
    }

//    private void writeHTMLHeader(PrintWriter pw, Map<Integer, ColumnInfo> inMap) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    public class ColumnInfo {

        boolean hidden;
        boolean centered;
        String headerTitle;
        Integer col;
        String elementName;

        public ColumnInfo(String inName, int inColumn) {
            hidden = false;
            centered = true;
            col = inColumn;
            headerTitle = inName.trim();
            headerTitle = (headerTitle.isEmpty() ? "Column" + col : headerTitle);

            elementName = headerTitle.replace(' ', '_').replace('#', '_').replace('/', '-');
            elementName = elementName.toLowerCase();
        }
    }

    public Map<Integer, ColumnInfo> getColumnInfoMap(Row inRow) {
        Map<Integer, ColumnInfo> retval = new HashMap<>();

        int lastColumn = inRow.getLastCellNum();

        for (int pos = 0; pos <= lastColumn; pos++) {
            // Checking for empty cells.
            String temp = getCellValue(inRow, pos);
            ColumnInfo ci = new ColumnInfo(temp, pos);
            retval.put(pos, ci);
        }
        return retval;
    }

    public final String getCellValue(Row inRow, int index) {
        String retval = "";
        if (inRow != null) {
            Object cell = inRow.getCell(index);

            if (cell != null) {
                retval = cell.toString();
            }
        }
        return retval;
    }

    public class InventoryItem {

        String[] values;
        Map<Integer, ColumnInfo> ciMap;

        public InventoryItem(Map<Integer, ColumnInfo> inMap) {
            ciMap = inMap;
        }

        public final boolean setValues(Row inRow) {
            boolean retval = false;
            values = new String[ciMap.size()];

            for (int idx = 0; idx < ciMap.size(); idx++) {
                values[idx] = getCellValue(inRow, idx);

                if (!values[idx].isEmpty()) {
                    retval = true;
                }
            }
            return retval;
        }

        public InventoryItem(XSSFRow inRow, Map<Integer, ColumnInfo> inMap) {
            this(inMap);
            this.setValues(inRow);
        }

        protected void writeXML(Element inRoot, Map<Integer, ColumnInfo> inMap) {
            Element item = inRoot.addElement("InventoryItem");

            for (int idx = 0; idx < inMap.size(); idx++) {
                ColumnInfo temp = inMap.get(idx);
                item.addElement(temp.elementName).addText(values[idx]);
            }
        }

        protected void writeHTML(PrintWriter inWriter, Map<Integer, ColumnInfo> inMap) {
            
            inWriter.println("\t\t<tr>");
            inWriter.println("\t\t\t<td class=\"details-control\"></td>");

            for (ColumnInfo i : inMap.values()) {
                inWriter.printf("\t\t\t<td align=\"center\">%s</td>\n", values[i.col]);
            }
            inWriter.println("\t\t<tr>");
        }
    }

    protected void writeHTMLHeader(PrintWriter inWriter, Map<Integer, ColumnInfo> inMap) {
        
        inWriter.println("\t\t<tr>");
        inWriter.println("\t\t\t<th><div align=\"center\"></div></th>");
        
        for (ColumnInfo i : inMap.values()) {
                inWriter.printf("\t\t\t<td align=\"center\">%s</td>\n", i.headerTitle);
        }
        inWriter.print("\t\t</tr>");
    }
    
    public boolean writerItemsXML(List<InventoryItem> listofInventory, File inOutput, Map<Integer, ColumnInfo> inMap) {
        boolean retval = false;
        try {
            Date date = new Date();
            Document document = DocumentHelper.createDocument();
            localProcessingInstruction pi = new localProcessingInstruction();
            pi.setTarget("xml-stylesheet");
            pi.setValue("type", "text/xsl");
            File xslOut = getTargetXSL(inOutput);
            pi.setValue("href", "xsl/" + xslOut.getName());
            document.add(pi);
            pi.setDocument(document);
            Element root = document.addElement("dataroot")
                .addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                .addAttribute("generated", date.toString());

            for (InventoryItem item : listofInventory) {
                item.writeXML(root, inMap);
            }

            OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter writer = new XMLWriter(new FileOutputStream(inOutput), format);
            writer.write(document);
            retval = true;
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        return retval;
    }

    public File getTargetXSL(File inOutput) {
        File xslOut;

        String xslName = inOutput.getName();
        int pos = xslName.lastIndexOf(".");
        xslName = xslName.substring(0, pos);
        xslName = xslName.concat(".xsl");

        xslOut = new File(inOutput.getParentFile(), "xsl");
        xslOut = new File(xslOut, xslName);

        return xslOut;
    }

    public File getTargetXHTML(File inOutput) {
        File xhtmlout;

        String xhtmlName = inOutput.getName();
        int pos = xhtmlName.lastIndexOf(".");
        xhtmlName = xhtmlName.substring(0, pos);
        xhtmlName = xhtmlName.concat(".xhtml");

        xhtmlout = new File(inOutput.getParentFile(), "xhtml");
        xhtmlout = new File(xhtmlout, xhtmlName);

        return xhtmlout;
    }

    public boolean writeItemsXSL(File inOutput, Map<Integer, ColumnInfo> inMap) {
        boolean retval = true;
        File xslOut = getTargetXSL(inOutput);
        // Read that template and write it out
        InputStream xslStream = this.getResourceStream("xsl/InventoryCatalog.xsl");

        if (xslStream == null) {
            return false;
        } else {
            FileWriter fWrite = null;
            try {
                fWrite = new FileWriter(xslOut);
                try {
                    XSSFRow rows = null;
                    InputStreamReader stream = new InputStreamReader(xslStream);
                    BufferedReader br = new BufferedReader(stream);
                    PrintWriter pw = new PrintWriter(fWrite);
                    String line = null;

                    while ((line = br.readLine()) != null) {
                        String temp = line.trim();

                        switch (temp) {
                            case "<!--HEADER-->":
                            case "<!--FOOTER-->":
                                writeHTMLHeader(pw, inMap);
                                break;
                            case "<!--Sentinal-->":
                                writeXSL(pw, inMap);
                                break;
                            default:
                                pw.println(line);
                                break;
                        }
                    }
                    pw.flush();
                    pw.flush();
                } catch (IOException ex) {
                    Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        fWrite.close();
                    } catch (IOException ex) {
                        Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fWrite.close();
                } catch (IOException ex) {
                    Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return retval;
    }

    // l. Changes from list to iterator
    // 2. Read the data from the iterator
    // 3. Write out the data to the .xhtml file
    public boolean writeItemsHTML(Iterator<Row> rowIterator, File inOutput) {
        boolean retval = true;
        // TODO code application logic here
        // Open an input stream of the	off the classpath
        InputStream templateStream = this.getResourceStream("template/ICTemplate.xhtml");

        if (templateStream == null) {
            return false;
        } else {
            FileWriter fWrite = null;
            try {
                Row tempRow = rowIterator.next();
                Map<Integer, ColumnInfo> ciMap = getColumnInfoMap(tempRow);
                InventoryItem tempItem = new InventoryItem(ciMap);
                File xHTML = getTargetXHTML(inOutput);

                InputStreamReader isr = new InputStreamReader(templateStream);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                fWrite = new FileWriter(inOutput);
                PrintWriter pw = new PrintWriter(fWrite);
                try {
                    // For each line in the template we'll be compared to the sentinal value
                    while ((line = br.readLine()) != null) {
                        String temp = line.trim();
                        switch (temp) {
                            // If the value is sentinal we'll process accordingly 
                            case "<!--GENERATED-DATE-HERE-->":
                                pw.print("<h3> Last Updated: ");
                                pw.print(new Date());
                                pw.println("</h3>");
                                break;
                            case "<!--TABLE-HEADER-->":
                            case "<!--TABLE-FOOTER-->":
                                writeHTMLHeader(pw, ciMap);
                                break;
                            case "<!--INVENTORY-ITEMS-HERE-->":
                                while (rowIterator.hasNext()) {
                                    tempRow = rowIterator.next();
                                    if (tempItem.setValues(tempRow)) {
                                        tempItem.writeHTML(pw, ciMap);
                                    }
                                }
                                break;
                            // If not we will write the line to System.out
                            default:
                                pw.println(line);
                                break;
                        }
                    }
                    pw.flush();
                    pw.flush();
                } catch (IOException ex) {
                    retval = false;
                    Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        if (fWrite != null) {
                            fWrite.close();
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                retval = false;
                Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
            return retval;
        }
    }

    public boolean writeItemHTML() {
        boolean retval = true;
        return retval;
    }

    public void prepDir(File inFile) {
        File targetDir = null;

        if (inFile != null) {
            if (inFile.isFile()) {
                targetDir = inFile.getParentFile();
            } else {
                targetDir = inFile;
            }
        }

        if (targetDir != null) {
            InputStream is = this.getResourceStream("RESOURCELIST.txt");
            List<String> ls = parseResources(is);

            for (String res : ls) {
                writeResources(res, targetDir);
            }
        }
    }

    protected InputStream getResourceStream(String inResname) {
        InputStream retval = ClassLoader.getSystemResourceAsStream(inResname);

        if (retval == null) {
            retval = ClassLoader.getSystemResourceAsStream("/" + inResname);
        }
        return retval;
    }

    public List<String> parseResources(InputStream inStream) {
        ArrayList<String> retval = new ArrayList<>();

        if (inStream != null) {
            InputStreamReader isr = new InputStreamReader(inStream);
            BufferedReader br = new BufferedReader(isr);

            String line = null;
            StringBuilder sb = new StringBuilder();

            try {
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
            }
            String temp = sb.toString();
            String[] arrayTokens = temp.split(";");
            retval.addAll(Arrays.asList(arrayTokens));
        }
        return retval;
    }

    public boolean writeResources(String inResName, File inTargetDir) {
        String tempResName = inResName.replaceAll("\\\\", "/");
        boolean retval = false;
        System.out.println("Writing .." + inResName);
        System.out.println("Writing.." + tempResName);

        System.out.println("Attempting" + tempResName);
        URL url = ClassLoader.getSystemResource(tempResName);
        InputStream is = null;
        if (url == null) {
            url = ClassLoader.getSystemResource("/" + tempResName);
            System.out.println("Attempting " + "/" + tempResName);
        }
        if (url != null) {
            System.out.println(url);
            try {
                is = url.openStream();
            } catch (IOException ex) {
                Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.exit(1);
        }

        if (is != null) {
            try {
                File targetfile = new File(inTargetDir, inResName);

                if (!targetfile.exists()) {
                    File parentDir = targetfile.getParentFile();
                    parentDir.mkdirs();
                    targetfile.createNewFile();

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(targetfile);
                        BufferedOutputStream boss = new BufferedOutputStream(fos);
                        BufferedInputStream bis = new BufferedInputStream(is);

                        byte[] bArray = new byte[1024];
                        int count;

                        while ((count = bis.read(bArray)) > 0) {
                            boss.write(bArray, 0, count);
                        }
                        boss.flush();
                        boss.flush();
                        retval = true;
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            if (fos != null) {
                                fos.close();
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(InventoryCatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retval;
    }

    public class localProcessingInstruction implements org.dom4j.ProcessingInstruction {

        private String target;
        private final Map<String, String> values;
        private Document document;

        public void setDocument(Document document) {
            this.document = document;
        }

        public Node clone() {
            return (Node) null;
        }

        public localProcessingInstruction() {
            values = new HashMap<>();
        }

        @Override
        public String getTarget() {
            return target;
        }

        @Override
        public void setTarget(String inTarget) {
            target = inTarget;
        }

        public void setValue(String inKey, String inValue) {
            if (inKey != null && inValue != null) {
                values.put(inKey, inValue);
            }
        }

        @Override
        public String getValue(String inKey) {
            String retval = null;

            if (inKey != null) {
                retval = values.get(inKey);
            }
            return retval;
        }

        @Override
        public Map<String, String> getValues() {
            return values;
        }

        @Override
        public String getText() {
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> me : values.entrySet()) {
                sb.append(me.getKey());
                sb.append("=\"");
                sb.append(me.getValue());
                sb.append("\" ");
            }
            return sb.toString();
        }

        @Override
        public void setValues(Map<String, String> map) {
        }

        @Override
        public boolean removeValue(String string) {
            return true;
        }

        @Override
        public boolean supportsParent() {
            return true;
        }

        @Override
        public Element getParent() {
            return null;
        }

        @Override
        public void setParent(Element elmnt) {
        }

        @Override
        public Document getDocument() {
            return document;
        }

        @Override
        public boolean isReadOnly() {
            return true;
        }

        @Override
        public boolean hasContent() {
            return true;
        }

        @Override
        public String getName() {
            return this.target;
        }

        @Override
        public void setName(String string) {
        }

        @Override
        public void setText(String string) {
        }

        @Override
        public String getStringValue() {
            return "";
        }

        @Override
        public String getPath() {
            return "";
        }

        @Override
        public String getPath(Element elmnt) {
            return "";
        }

        @Override
        public String getUniquePath() {
            return "";
        }

        @Override
        public String getUniquePath(Element elmnt) {
            return "";
        }

        @Override
        public String asXML() {
            return "";
        }

        @Override
        public void write(Writer writer) throws IOException {
        }

        @Override
        public short getNodeType() {
            return Node.PROCESSING_INSTRUCTION_NODE;
        }

        @Override
        public String getNodeTypeName() {
            return "";
        }

        @Override
        public Node detach() {
            return null;
        }

        @Override
        public List<Node> selectNodes(String string) {
            return null;
        }

        @Override
        public Object selectObject(String string) {
            return null;
        }

        @Override
        public List<Node> selectNodes(String string, String string1) {
            return null;
        }

        @Override
        public List<Node> selectNodes(String string, String string1, boolean bln) {
            return null;
        }

        @Override
        public Node selectSingleNode(String string) {
            return null;
        }

        @Override
        public String valueOf(String string) {
            return "";
        }

        @Override
        public Number numberValueOf(String string) {
            return null;
        }

        @Override
        public boolean matches(String string) {
            return true;
        }

        @Override
        public XPath createXPath(String string) throws InvalidXPathException {
            return null;
        }

        @Override
        public Node asXPathResult(Element elmnt) {
            return null;
        }

        @Override
        public void accept(Visitor vstr) {
        }
    }
}
